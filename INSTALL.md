<!--
  Copyright (C) 2020,2021  CismonX <admin@cismon.net>
  
  Copying and distribution of this file, with or without modification, are
  permitted in any medium without royalty, provided the copyright notice and
  this notice are preserved. This file is offered as-is, without any warranty.
-->

Installing ctlseqs
==================

 Copy Code to Your Project
 -------------------------

  The code of ctlseqs is simple, and can be used out-of-the-box. Just copy
  ctlseqs.h and ctlseqs.c to your project and build it with other code.

  Requires an ISO C99 and POSIX.1-2001 compliant C implementation.

 Build and Install From Source
 -----------------------------

  Alternatively, ctlseqs can be built into a shared/static library with
  GNU Autotools (Autoconf, Automake, Libtool and Autoconf Archive).

    autoreconf --install
    ./configure
    make

  Optionally, you can run tests (requires DejaGnu) and install the library.

    make check
    make install

  Unix man pages are not automatically installed with `make install`, and
  should be installed manually with `make install-man`.
