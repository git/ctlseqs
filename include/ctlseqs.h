/**
 * ctlseqs.h - helper library for control sequences
 *
 * Copyright (C) 2020,2021  CismonX <admin@cismon.net>
 *
 * This file is part of the ctlseqs library.
 *
 * ctlseqs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ctlseqs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ctlseqs.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CTLSEQS_H_
#define CTLSEQS_H_

#define CTLSEQS_VERSION_MAJOR  0x00
#define CTLSEQS_VERSION_MINOR  0x01
#define CTLSEQS_VERSION_PATCH  0x00
#define CTLSEQS_VERSION_ID     0x000100
#define CTLSEQS_VERSION_STRING "0.1.0"

/* C0 Control Functions */

// Null
#define CTLSEQS_NUL "\x00"
// Start of Heading
#define CTLSEQS_SOH "\x01"
// Start of Text
#define CTLSEQS_STX "\x02"
// End of Text
#define CTLSEQS_ETX "\x03"
// End of Transmission
#define CTLSEQS_EOT "\x04"
// Enquiry
#define CTLSEQS_ENQ "\x05"
// Acknowledge
#define CTLSEQS_ACK "\x06"
// Bell
#define CTLSEQS_BEL "\x07"
// Backspace
#define CTLSEQS_BS  "\x08"
// Horizontal Tab
#define CTLSEQS_HT  "\x09"
// Line Feed
#define CTLSEQS_LF  "\x0a"
// Vertical Tab
#define CTLSEQS_VT  "\x0b"
// Form Feed or New Page
#define CTLSEQS_FF  "\x0c"
// Carriage Return
#define CTLSEQS_CR  "\x0d"
// Shift Out
#define CTLSEQS_SO  "\x0e"
// Shift In
#define CTLSEQS_SI  "\x0f"
// Data Link Escape
#define CTLSEQS_DLE "\x10"
// Device Control 1
#define CTLSEQS_DC1 "\x11"
// Device Control 2
#define CTLSEQS_DC2 "\x12"
// Device Control 3
#define CTLSEQS_DC3 "\x13"
// Device Control 4
#define CTLSEQS_DC4 "\x14"
// Negative Acknowledgement
#define CTLSEQS_NAK "\x15"
// Synchronous Idle
#define CTLSEQS_SYN "\x16"
// End of Transmission Block
#define CTLSEQS_ETB "\x17"
// Cancel
#define CTLSEQS_CAN "\x18"
// End of Medium
#define CTLSEQS_EM  "\x19"
// Substitute
#define CTLSEQS_SUB "\x1a"
// Escape
#define CTLSEQS_ESC "\x1b"
// File Separator
#define CTLSEQS_FS  "\x1c"
// Group Separator
#define CTLSEQS_GS  "\x1d"
// Record Separator
#define CTLSEQS_RS  "\x1e"
// Unit Separator
#define CTLSEQS_US  "\x1f"
// Space
#define CTLSEQS_SP  "\x20"
// Delete
#define CTLSEQS_DEL "\x7f"

/* C1 Control Functions (2-character 7-bit representation) */

// Break Permitted Here
#define CTLSEQS_BPH CTLSEQS_ESC "B"
// No Break Here
#define CTLSEQS_NBH CTLSEQS_ESC "C"
// Index
#define CTLSEQS_IND CTLSEQS_ESC "D"
// Next Line
#define CTLSEQS_NEL CTLSEQS_ESC "E"
// Start of Selected Area
#define CTLSEQS_SSA CTLSEQS_ESC "F"
// End of Selected Area
#define CTLSEQS_ESA CTLSEQS_ESC "G"
// Character Tabulation Set
#define CTLSEQS_HTS CTLSEQS_ESC "H"
// Line Tabulation Set
#define CTLSEQS_VTS CTLSEQS_ESC "J"
// Partial Line Forward
#define CTLSEQS_PLD CTLSEQS_ESC "K"
// Partial Line Backward
#define CTLSEQS_PLU CTLSEQS_ESC "L"
// Reverse Index
#define CTLSEQS_RI  CTLSEQS_ESC "M"
// Single Shift 2
#define CTLSEQS_SS2 CTLSEQS_ESC "N"
// Single Shift 3
#define CTLSEQS_SS3 CTLSEQS_ESC "O"
// Device Control String
#define CTLSEQS_DCS CTLSEQS_ESC "P"
// Private Use 1
#define CTLSEQS_PU1 CTLSEQS_ESC "Q"
// Private Use 2
#define CTLSEQS_PU2 CTLSEQS_ESC "R"
// Set Transmit State
#define CTLSEQS_STS CTLSEQS_ESC "S"
// Cancel Character
#define CTLSEQS_CCH CTLSEQS_ESC "T"
// Message Waiting
#define CTLSEQS_MW  CTLSEQS_ESC "U"
// Start of Guarded Area
#define CTLSEQS_SPA CTLSEQS_ESC "V"
// End of Guarded Area
#define CTLSEQS_EPA CTLSEQS_ESC "W"
// Start of String
#define CTLSEQS_SOS CTLSEQS_ESC "X"
// Character Tabulation with Justification
#define CTLSEQS_HTJ CTLSEQS_ESC "Y"
// Single Character Introducer
#define CTLSEQS_SCI CTLSEQS_ESC "Z"
// Control Sequence Introducer
#define CTLSEQS_CSI CTLSEQS_ESC "["
// String Terminator
#define CTLSEQS_ST  CTLSEQS_ESC "\\"
// Operating System Command
#define CTLSEQS_OSC CTLSEQS_ESC "]"
// Privacy Message
#define CTLSEQS_PM  CTLSEQS_ESC "^"
// Application Program Command
#define CTLSEQS_APC CTLSEQS_ESC "_"

/* Controls beginning with ESC */

// 7-bit controls
#define CTLSEQS_S7C1T()        CTLSEQS_ESC " F"
// 8-bit controls
#define CTLSEQS_S8C1T()        CTLSEQS_ESC " G"
// DEC double-height line, top half
#define CTLSEQS_DECDHL_TOP()   CTLSEQS_ESC "#3"
// DEC double-height line, bottom half
#define CTLSEQS_DECDHL_BOTOM() CTLSEQS_ESC "#4"
// DEC single-width line
#define CTLSEQS_DECSWL()       CTLSEQS_ESC "#5"
// DEC double-width line
#define CTLSEQS_DECDWL()       CTLSEQS_ESC "#6"
// DEC Screen Alignment Test
#define CTLSEQS_DECALN()       CTLSEQS_ESC "#8"
// Back Index
#define CTLSEQS_DECBI()        CTLSEQS_ESC "6"
// Save Cursor
#define CTLSEQS_DECSC()        CTLSEQS_ESC "7"
// Restore Cursor
#define CTLSEQS_DECRC()        CTLSEQS_ESC "8"
// Forward Index
#define CTLSEQS_DECFI()        CTLSEQS_ESC "9"
// Application Keypad
#define CTLSEQS_DECKPAM()      CTLSEQS_ESC "="
// Normal Keypad
#define CTLSEQS_DECKPNM()      CTLSEQS_ESC ">"
// Full Reset
#define CTLSEQS_RIS()          CTLSEQS_ESC "c"

/* Device-Control functions */

// User-Defined Keys
#define CTLSEQS_DECUDK(i1, i2, s) CTLSEQS_DCS i1 ";" i2 "|" s CTLSEQS_ST
// Request Status String
#define CTLSEQS_DECRQSS(s)        CTLSEQS_DCS "$q" s CTLSEQS_ST
// Restore presentation status
#define CTLSEQS_DECRSPS(i, s)     CTLSEQS_DCS i "$t" s CTLSEQS_ST
// Request resource values
#define CTLSEQS_XTGETXRES(s)      CTLSEQS_DCS "+Q" s CTLSEQS_ST
// Set Termcap/Terminfo Data
#define CTLSEQS_XTSETTCAP(s)      CTLSEQS_DCS "+p" s CTLSEQS_ST
// Request Termcap/Terminfo String
#define CTLSEQS_XTGETTCAP(s)      CTLSEQS_DCS "+q" s CTLSEQS_ST

/* Control Sequences */

// Insert $n (Blank) Character(s)
#define CTLSEQS_ICH(n)                             CTLSEQS_CSI n "@"
// Shift left $n columns(s)
#define CTLSEQS_SL(n)                              CTLSEQS_CSI n " @"
// Cursor Up $n Times
#define CTLSEQS_CUU(n)                             CTLSEQS_CSI n "A"
// Shift right $n columns(s)
#define CTLSEQS_SR(n)                              CTLSEQS_CSI n " A"
// Cursor Down $n Times
#define CTLSEQS_CUD(n)                             CTLSEQS_CSI n "B"
// Cursor Forward $n Times
#define CTLSEQS_CUF(n)                             CTLSEQS_CSI n "C"
// Cursor Backward $n Times
#define CTLSEQS_CUB(n)                             CTLSEQS_CSI n "D"
// Cursor Next Line $n Times
#define CTLSEQS_CNL(n)                             CTLSEQS_CSI n "E"
// Cursor Preceding Line $n Times
#define CTLSEQS_CPL(n)                             CTLSEQS_CSI n "F"
// Cursor Character Absolute
#define CTLSEQS_CHA(row)                           CTLSEQS_CSI row "G"
// Cursor Position
#define CTLSEQS_CUP(row, col)                      CTLSEQS_CSI row ";" col "H"
// Cursor Forward Tabulation $n tab stops
#define CTLSEQS_CHT(n)                             CTLSEQS_CSI n "I"
// Erase in Display
#define CTLSEQS_ED(i)                              CTLSEQS_CSI i "J"
// Erase in Display
#define CTLSEQS_DECSED(i)                          CTLSEQS_CSI "?" i "J"
// Erase in Line
#define CTLSEQS_EL(i)                              CTLSEQS_CSI i "K"
// Erase in Line
#define CTLSEQS_DECSEL(i)                          CTLSEQS_CSI "?" i "K"
// Insert $n Line(s)
#define CTLSEQS_IL(n)                              CTLSEQS_CSI n "L"
// Delete $n Line(s)
#define CTLSEQS_DL(n)                              CTLSEQS_CSI n "M"
// Delete $n Character(s)
#define CTLSEQS_DCH(n)                             CTLSEQS_CSI n "P"
// Scroll up $n Lines
#define CTLSEQS_SU(n)                              CTLSEQS_CSI n "S"
// Set or request graphics attribute
#define CTLSEQS_XTSMGRAPHICS(i1, i2, is)           \
        CTLSEQS_CSI "?" i1 ";" i2 ";" is "S"
// Scroll Down $n Line(s)
#define CTLSEQS_SD(n)                              CTLSEQS_CSI n "T"
// Initiate highlight mouse tracking
#define CTLSEQS_XTHIMOUSE(f, x, y, fr, lr)         \
        CTLSEQS_CSI f ";" x ";" y ";" fr ";" lr "T"
// Reset title mode features to default value
#define CTLSEQS_XTRMTITLE(is)                      CTLSEQS_CSI ">" is "T"
// Erase $n Character(s)
#define CTLSEQS_ECH(n)                             CTLSEQS_CSI n "X"
// Cursor Backward Tabulation $n tab stops
#define CTLSEQS_CBT(n)                             CTLSEQS_CSI n "Z"
// Character Position Absolute
#define CTLSEQS_HPA(n)                             CTLSEQS_CSI n "`"
// Character Position Relative
#define CTLSEQS_HPR(n)                             CTLSEQS_CSI n "a"
// Repeat the preceding graphic character $n times
#define CTLSEQS_REP(n)                             CTLSEQS_CSI n "b"
// Send Device Attributes
#define CTLSEQS_PRIMARY_DA()                       CTLSEQS_CSI "c"
// Send Device Attributes
#define CTLSEQS_TERTIARY_DA()                      CTLSEQS_CSI "=0c"
// Send Device Attributes
#define CTLSEQS_SECONDARY_DA()                     CTLSEQS_CSI ">c"
// Line Position Absolute
#define CTLSEQS_VPA(row)                           CTLSEQS_CSI row "d"
// Line Position Relative
#define CTLSEQS_VPR(rows)                          CTLSEQS_CSI rows "e"
// Horizontal and Vertical Position
#define CTLSEQS_HVP(row, col)                      CTLSEQS_CSI row ";" col "f"
// Tab Clear
#define CTLSEQS_TBC(i)                             CTLSEQS_CSI i "g"
// Set Mode
#define CTLSEQS_SM(i)                              CTLSEQS_CSI i "h"
// DEC Private Mode Set
#define CTLSEQS_DECSET(i)                          CTLSEQS_CSI "?" i "h"
// Media Copy
#define CTLSEQS_MC(i)                              CTLSEQS_CSI i "i"
// Media Copy, DEC-specific
#define CTLSEQS_MC_DEC(i)                          CTLSEQS_CSI "?" i "i"
// Reset Mode
#define CTLSEQS_RM(i)                              CTLSEQS_CSI i "l"
// DEC Private Mode Reset
#define CTLSEQS_DECRST(i)                          CTLSEQS_CSI "?" i "l"
// Character Attributes
#define CTLSEQS_SGR(i)                             CTLSEQS_CSI i "m"
// Set/reset key modifier options
#define CTLSEQS_XTMODKEYS(is)                      CTLSEQS_CSI ">" is "m"
// Device Status Report
#define CTLSEQS_DSR(i)                             CTLSEQS_CSI i "n"
// Disable key modifier options
#define CTLSEQS_XTMODKEYS_DISABLE(i)               CTLSEQS_CSI ">" i "n"
// Device Status Report
#define CTLSEQS_DSR_DEC(i)                         CTLSEQS_CSI "?" i "n"
// Set resource value pointerMode
#define CTLSEQS_XTSMPOINTER(i)                     CTLSEQS_CSI ">" i "p"
// Soft terminal reset
#define CTLSEQS_DECSTR()                           CTLSEQS_CSI "!p"
// Set conformance level
#define CTLSEQS_DECSCL(i1, i2)                     CTLSEQS_CSI i1 ";" i2 "\"p"
// Request ANSI mode
#define CTLSEQS_DECRQM_ANSI(i)                     CTLSEQS_CSI i "$p"
// Request DEC private mode
#define CTLSEQS_DECRQM(i)                          CTLSEQS_CSI "?" i "$p"
// Report xterm name and version
#define CTLSEQS_XTVERSION()                        CTLSEQS_CSI ">0q"
// Load LEDs
#define CTLSEQS_DECLL(i)                           CTLSEQS_CSI i "q"
// Set cursor style
#define CTLSEQS_DECSCUSR(i)                        CTLSEQS_CSI i " q"
// Select character protection attribute
#define CTLSEQS_DECSCA(i)                          CTLSEQS_CSI i "\"q"
// Set Scrolling Region
#define CTLSEQS_DECSTBM(top, btm)                  CTLSEQS_CSI top ";" btm "r"
// Restore DEC Private Mode Values
#define CTLSEQS_XTRESTORE(is)                      CTLSEQS_CSI "?" is "r"
// Change Attributes in Rectangular Area
#define CTLSEQS_DECCARA(t, l, b, r, is)            \
        CTLSEQS_CSI t ";" l ";" b ";" r ";" is "$r"
// Save cursor
#define CTLSEQS_SCOSC()                            CTLSEQS_CSI "s"
// Set left and right margins
#define CTLSEQS_DECSLRM(l, r)                      CTLSEQS_CSI l ";" r "s"
// Save DEC Private Mode Values
#define CTLSEQS_XTSAVE(is)                         CTLSEQS_CSI "?" is "s"
// Window manipulation
#define CTLSEQS_XTWINOPS(is)                       CTLSEQS_CSI is "t"
// Set one or more features of the title modes
#define CTLSEQS_XTSMTITLE(is)                      CTLSEQS_CSI ">" is "t"
// Set warning-bell volume
#define CTLSEQS_DECSWBV(i)                         CTLSEQS_CSI i " t"
// Reverse Attributes in Rectangular Area
#define CTLSEQS_DECRARA(t, l, b, r, is)            \
        CTLSEQS_CSI t ";" l ";" b ";" r ";" is "$t"
// Restore cursor
#define CTLSEQS_SCORC()                            CTLSEQS_CSI "u"
// Set margin-bell volume
#define CTLSEQS_DECSMBV(i)                         CTLSEQS_CSI i " u"
// Copy Rectangular Area
#define CTLSEQS_DECCRA(t, l, b, r, sp, dt, dl, dp) \
        CTLSEQS_CSI t ";" l ";" b ";" r ";" sp ";" dt ";" dl ";" dp "$v"
// Request presentation state report
#define CTLSEQS_DECRQPSR(i)                        CTLSEQS_CSI i "$w"
// Enable Filter Rectangle
#define CTLSEQS_DECEFR(t, l, b, r)                 \
        CTLSEQS_CSI t ";" l ";" b ";" r "'w"
// Request Terminal Parameters
#define CTLSEQS_DECREQTPARM(i)                     CTLSEQS_CSI i "x"
// Select Attribute Change Extent
#define CTLSEQS_DECSACE(i)                         CTLSEQS_CSI i "*x"
// Fill Rectangular Area
#define CTLSEQS_DECFRA(c, t, l, b, r)              \
        CTLSEQS_CSI c ";" t ";" l ";" b ";" r "$x"
// Select checksum extension
#define CTLSEQS_XTCHECKSUM(i)                      CTLSEQS_CSI i "#y"
// Request Checksum of Rectangular Area
#define CTLSEQS_DECRQCRA(id, p, t, l, b, r)        \
        CTLSEQS_CSI id ";" p ";" t ";" l ";" b ";" r "*y"
// Enable Locator Reporting
#define CTLSEQS_DECELR(i1, i2)                     CTLSEQS_CSI i1 ";" i2 "'z"
// Erase Rectangular Area
#define CTLSEQS_DECERA(t, l, b, r)                 \
        CTLSEQS_CSI t ";" l ";" b ";" r "$z"
// Select Locator Events
#define CTLSEQS_DECSLE(is)                         CTLSEQS_CSI is "'{"
// Push video attributes onto stack
#define CTLSEQS_XTPUSHSGR(is)                      CTLSEQS_CSI is "#{"
// Selective Erase Rectangular Area
#define CTLSEQS_DECSERA(t, l, b, r)                \
        CTLSEQS_CSI t ";" l ";" b ";" r "${"
// Report selected graphic rendition
#define CTLSEQS_XTREPORTSGR(t, l, b, r)            \
        CTLSEQS_CSI t ";" l ";" b ";" r "$|"
// Select columns per page
#define CTLSEQS_DECSCPP(i)                         CTLSEQS_CSI i "$|"
// Request Locator Position
#define CTLSEQS_DECRQLP()                          CTLSEQS_CSI "'|"
// Select $n number of lines per screen
#define CTLSEQS_DECSNLS(n)                         CTLSEQS_CSI n "*|"
// Pop video attributes from stack
#define CTLSEQS_XTPOPSGR()                         CTLSEQS_CSI "#}"
// Insert $n Column(s)
#define CTLSEQS_DECIC(n)                           CTLSEQS_CSI n "'}"
// Delete $n Column(s)
#define CTLSEQS_DECDC(n)                           CTLSEQS_CSI n "'~"

/* Operating System Commands */

// Set Text Parameters
#define CTLSEQS_OSC_SEQ(i, s) CTLSEQS_OSC i ";" s CTLSEQS_ST

/* Response messages */

// DECRQSS response message
#define CTLSEQS_RESP_DECRQSS(n, s)              CTLSEQS_DCS n "$r" s CTLSEQS_ST
// XTGETXRES response message
#define CTLSEQS_RESP_XTGETXRES(n, s)            CTLSEQS_DCS n "+R" s CTLSEQS_ST
// XTGETTCAP response message
#define CTLSEQS_RESP_XTGETTCAP(n, s)            CTLSEQS_DCS n "+r" s CTLSEQS_ST
// Primary DA response message
#define CTLSEQS_RESP_PRIMARY_DA(ns)             CTLSEQS_CSI "?" ns "c"
// Secondary DA response message
#define CTLSEQS_RESP_SECONDARY_DA(n1, n2, n3)   \
        CTLSEQS_CSI ">" n1 ";" n2 ";" n3 "c"
// DECLRP response message
#define CTLSEQS_RESP_DECXCPR(n1, n2)            CTLSEQS_CSI "?" n1 ";" n2 "R"
// DSR response message
#define CTLSEQS_RESP_DSR(ns)                    CTLSEQS_CSI "?" ns "n"
// DECRQM response message (ANSI mode)
#define CTLSEQS_RESP_DECRQM_ANSI(n1, n2)        CTLSEQS_CSI n1 ";" n2 "$y"
// DECRQM response message (DEC private mode)
#define CTLSEQS_RESP_DECRQM(n1, n2)             CTLSEQS_CSI "?" n1 ";" n2 "$y"
// XTVERSION response message
#define CTLSEQS_RESP_XTVERSION(s)               CTLSEQS_DCS ">|" s CTLSEQS_ST
// DECCIR response message
#define CTLSEQS_RESP_DECCIR(s)                  CTLSEQS_DCS "1$u" s CTLSEQS_ST
// DECTABSR response message
#define CTLSEQS_RESP_DECTABSR(s)                CTLSEQS_DCS "2$u" s CTLSEQS_ST
// DECRQCRA response message
#define CTLSEQS_RESP_DECRQCRA(n, s)             CTLSEQS_DCS n "!~" s CTLSEQS_ST
// DECRQLP response message
#define CTLSEQS_RESP_DECRQLP(e, b, row, col, p) \
        CTLSEQS_CSI e ";" b ";" row ";" col ";" p "&w"
// Mouse response in SGR mouse mode
#define CTLSEQS_RESP_SGR_MOUSE(n, col, row, c)  \
        CTLSEQS_CSI "<" n ";" col ";" row c

/// PC-Style Function Keys

// Up arrow key
#define CTLSEQS_KEY_UP()     CTLSEQS_CSI "A"
// Down arrow key
#define CTLSEQS_KEY_DOWN()   CTLSEQS_CSI "B"
// Right arrow key
#define CTLSEQS_KEY_RIGHT()  CTLSEQS_CSI "C"
// Left arrow key
#define CTLSEQS_KEY_LEFT()   CTLSEQS_CSI "D"
// Home key
#define CTLSEQS_KEY_HOME()   CTLSEQS_CSI "H"
// End key
#define CTLSEQS_KEY_END()    CTLSEQS_CSI "F"
// Insert key
#define CTLSEQS_KEY_INSERT() CTLSEQS_CSI "2~"
// Delete key
#define CTLSEQS_KEY_DELETE() CTLSEQS_CSI "3~"
// Page Up key
#define CTLSEQS_KEY_PGUP()   CTLSEQS_CSI "5~"
// Page Down key
#define CTLSEQS_KEY_PGDN()   CTLSEQS_CSI "6~"
// F1 key
#define CTLSEQS_KEY_F1()     CTLSEQS_SS3 "P"
// F2 key
#define CTLSEQS_KEY_F2()     CTLSEQS_SS3 "Q"
// F3 key
#define CTLSEQS_KEY_F3()     CTLSEQS_SS3 "R"
// F4 key
#define CTLSEQS_KEY_F4()     CTLSEQS_SS3 "S"
// F5 key
#define CTLSEQS_KEY_F5()     CTLSEQS_CSI "15~"
// F6 key
#define CTLSEQS_KEY_F6()     CTLSEQS_CSI "17~"
// F7 key
#define CTLSEQS_KEY_F7()     CTLSEQS_CSI "18~"
// F8 key
#define CTLSEQS_KEY_F8()     CTLSEQS_CSI "19~"
// F9 key
#define CTLSEQS_KEY_F9()     CTLSEQS_CSI "20~"
// F10 key
#define CTLSEQS_KEY_F10()    CTLSEQS_CSI "21~"
// F11 key
#define CTLSEQS_KEY_F11()    CTLSEQS_CSI "23~"
// F12 key
#define CTLSEQS_KEY_F12()    CTLSEQS_CSI "24~"

#ifdef __cplusplus
#  include <cstddef>
#else
#  include <stddef.h>
#endif  // __cplusplus

#include <sys/types.h>

/* Placeholders */

// CSI Parameter Bytes, numbers only
#define CTLSEQS_PH_NUM       "\x0e"
// CSI Parameter Bytes, multiple numbers separated by semicolon
#define CTLSEQS_PH_NUMS      "\x0f"
// String, printable characters only
#define CTLSEQS_PH_STR       "\x10"
// Command String
#define CTLSEQS_PH_CMDSTR    "\x11"
// CSI Parameter Bytes
#define CTLSEQS_PH_CSI_PARAM "\x12"
// CSI Intermediate Bytes
#define CTLSEQS_PH_CSI_INTMD "\x13"
// Printable characters representing a hexadecimal number
#define CTLSEQS_PH_HEXNUM    "\x14"
// Character String
#define CTLSEQS_PH_CHRSTR    "\x15"

/* Reader option flags */

// Do not poll() before read()
#define CTLSEQS_READER_NO_POLL           (1 << 0)
// Save successfully matched sequence to buffer
#define CTLSEQS_READER_SAVE_MATCHED_SEQS (1 << 1)

/* Function return status codes */

#define CTLSEQS_OK       0
#define CTLSEQS_NOMEM   -1
#define CTLSEQS_ERROR   -2
#define CTLSEQS_EOF     -3
#define CTLSEQS_TIMEOUT -4
#define CTLSEQS_NOMATCH -5  // Not matching given patterns
#define CTLSEQS_PARTIAL -6  // Parially a control sequence
#define CTLSEQS_NOSEQ   -7  // Not a control sequence
#define CTLSEQS_INTR    -8  // Interrupted by signal

struct ctlseqs_matcher;

struct ctlseqs_reader;

struct ctlseqs_matcher_options {
    char const *const *patterns;
    size_t             npatterns;
};

struct ctlseqs_reader_options {
    union ctlseqs_value *result;
    size_t               maxlen;
    int                  fd;
    unsigned             flags;
};

union ctlseqs_value {
    char const    *str;
    size_t         len;
    unsigned long  num;
};

#ifdef __cplusplus
extern "C" {

using std::size_t;
#endif  // __cplusplus

struct ctlseqs_matcher *
ctlseqs_matcher_init();

int
ctlseqs_matcher_config(
    struct ctlseqs_matcher               *matcher,
    struct ctlseqs_matcher_options const *options
);

void
ctlseqs_matcher_free(
    struct ctlseqs_matcher *matcher
);

ssize_t
ctlseqs_match(
    struct ctlseqs_matcher const *matcher,
    char                   const *str,
    size_t                        str_len,
    union ctlseqs_value          *result
);

struct ctlseqs_reader *
ctlseqs_reader_init();

int
ctlseqs_reader_config(
    struct ctlseqs_reader               *reader,
    struct ctlseqs_reader_options const *options
);

ssize_t
ctlseqs_read(
    struct ctlseqs_reader        *reader,
    struct ctlseqs_matcher const *matcher,
    int                           timeout
);

void
ctlseqs_purge(
    struct ctlseqs_reader *reader,
    size_t                 nbytes
);

void
ctlseqs_reader_free(
    struct ctlseqs_reader *reader
);

#ifdef __cplusplus
}
#endif  // __cplusplus

#endif  // !CTLSEQS_H_
