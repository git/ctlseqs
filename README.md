<!--
  Copyright (C) 2020,2021  CismonX <admin@cismon.net>
  
  Copying and distribution of this file, with or without modification, are
  permitted in any medium without royalty, provided the copyright notice and
  this notice are preserved. This file is offered as-is, without any warranty.
-->

ctlseqs
=======

  The ctlseqs library provides C API for handling ECMA-35/ECMA-48 compatible
  control functions.

  ctlseqs is free software, licensed under the terms of the GNU General Public
  License, either version 3, or any later version of the license. See COPYING
  for details.

  See INSTALL.md for instructions on how to build and install ctlseqs.

  See `info ctlseqs` for documentation.

  Visit the [project homepage] for online documentation, mailing lists,
  releases, and everything else about ctlseqs.


<!-- Reference Links -->

[project homepage]: https://savannah.nongnu.org/projects/ctlseqs
